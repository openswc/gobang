GobangC : main.o game.o draw.o menu.o cpu.o
	./counter.sh
	gcc -o GobangC main.o game.o draw.o menu.o cpu.o  `pkg-config --cflags --libs gtk+-3.0`
	@echo "To Launch Game type : ./GobangC"

main.o : ./src/main.c ./src/game.h
	gcc -c ./src/main.c `pkg-config --cflags --libs gtk+-3.0`

game.o : ./src/*
	gcc -c ./src/game.c `pkg-config --cflags --libs gtk+-3.0`

draw.o : ./src/*
	gcc -c ./src/draw.c `pkg-config --cflags --libs gtk+-3.0`

menu.o : ./src/*
	gcc -c ./src/menu.c `pkg-config --cflags --libs gtk+-3.0`

cpu.o : ./src/*
	gcc -c ./src/cpu.c `pkg-config --cflags --libs gtk+-3.0`

clean:
	rm -f *.o GobangC
/*
Game GobangC ver 1.0 

Copyright (C) 2012  feitian (feitian16@gmail.com)
Project home page : http://gobangft.sourceforge.net

Game Tictactoe
Project home page : http://tictactoegtk.sourceforge.net

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

/*

file:    main.c
author:  feitian16
date:    2012-05-01
Version: 1.0.0
Email:   feitian16@gmail.com
history:

*/

#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

#include "build.h"
#include "define.h"

#include "game.h"
#include "cpu.h"
#include "draw.h"
#include "menu.h"

//#include "game.c"


int main(int argc,char *argv[])
{

	g_printf("***\n\n");
	g_printf("%s Build ",release_version);
	g_printf(build);
	g_printf(" Copyright (C) 2012  feitian16 (feitian16@gmail.com)\nProject home page : http://tictactoegtk.sourceforge.net\n");
	g_printf("GobangC is free software; you can redistribute it and/or modify\nit under the terms of the GNU General Public License as published by \nthe Free Software Foundation; either version 2 of the License, or \n (at your option) any later version.\n\nTicTacToeGTK is distributed in the hope that it will be useful, \nbut WITHOUT ANY WARRANTY; without even the implied warranty of \nMERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the \nGNU General Public License for more details.\n\nYou should have received a copy of the GNU General Public License \nalong with Nautilus; if not, write to the Free Software Foundation, Inc., \n51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA");
	g_printf("\n\n***\n\n");

	//Debug message
	#if debug
	g_printf(window_title);
	g_printf("\nDebug version\n");
	#endif

	//GTK init
	gtk_init(&argc,&argv);
	         
	//Init Game
	init_game();

	window=gtk_window_new(GTK_WINDOW_TOPLEVEL);//Create Main Window
	gtk_widget_set_size_request(window,605,670);//Set size of Main Window
	gtk_window_set_title(GTK_WINDOW(window),window_title);//Set WIndow title
	gtk_window_set_policy(GTK_WINDOW(window),FALSE,FALSE,TRUE);//Set Window Policy
        gtk_window_set_position(GTK_WINDOW(window),GTK_WIN_POS_CENTER);//Set Window Position
	
	//Config destroy signal
	g_signal_connect(G_OBJECT(window),"delete_event",G_CALLBACK(delete_event),NULL);
	g_signal_connect(G_OBJECT(window),"destroy",G_CALLBACK(destroy),NULL);
	         
	//Create Event box
	ebox=gtk_event_box_new();
	darea=gtk_drawing_area_new();
	//darea=gtk_widget_set_size_request(darea,100,100);
	         
	//Config Signals
	g_signal_connect (G_OBJECT (darea), "expose_event",G_CALLBACK (redraw), NULL);//Redraw signal              
        g_signal_connect (G_OBJECT (ebox),"button_press_event",G_CALLBACK (Game_action),darea);//Signal form user

	//Status Bar
	status_bar=gtk_statusbar_new();
	gtk_widget_set_size_request(status_bar,300,20);
	stbar_id=gtk_statusbar_get_context_id(GTK_STATUSBAR(status_bar),"Info");
	         
	//Menu
	menu_bar=get_menubar_menu(window);
	//VBox
	vbox=gtk_vbox_new(FALSE,10);
	
	//packing widgets
	//Adding Menu
	gtk_box_pack_start (GTK_BOX (vbox), menu_bar, FALSE, FALSE, 0);

	//Adding DAREA
	gtk_container_add(GTK_CONTAINER(ebox),darea);
	gtk_container_add(GTK_CONTAINER(vbox),ebox);

	gtk_box_pack_start(GTK_BOX(vbox),status_bar,FALSE,FALSE,0);
	//Adding VBox on main window
	gtk_container_add(GTK_CONTAINER(window),vbox);
	//Show widget on screen
	gtk_widget_show_all(window);
	gtk_statusbar_push(GTK_STATUSBAR(status_bar),stbar_id,release_version);
	         
	//Show login screen :)
	login();
	         
	//Init Colors
	initcolors();
	
	gtk_main();
	return 0;
}

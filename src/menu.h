#ifndef menu_h
#define menu_h

/*

file:    menu.h - header file
author:  feitian16
date:    2012-04-24
Version: 1.0.0
Email:   feitian16@gmail.com

*/

/*
Description of Interface :
When game need a CPU move it call cpumove() function.
Variables to access of function cpumove() :
grid[3][3] - array 3x3 of char, contain position of players chars
cpuchar - type : char, CPU player char
usrchar - type : char, Player char

Important :
CPU need to make ONE  move of CPU Player
*/

GtkWidget* get_menubar_menu( GtkWidget *);
void callnewgame(GtkWidget *,gpointer);
gint newgame_confirm(void);
void calloption( GtkWidget *,gpointer);
void callexit( GtkWidget *,gpointer);
void call_about(GtkWidget *,gpointer);
void call_about_website(GtkWidget *,gpointer);

#endif

#ifndef define_h
#define define_h

/*

file:    define.h - header file
author:  feitian16
date:    2012-04-24
Version: 1.0.0
Email:   feitian16@gmail.com

*/


#define debug 1 //Debug Switch : 0 - Debug messages OFF; 1 - Debug messages ON

#define release_version "GobangC 1.0"

#define build_date "April 24th 2012"

#define window_title release_version

#define Name_msg "Enter your name : "
#define Mode_msg "Game Mode: "
#define Chess_msg "Choice char : "
#define Player2name_msg "If you choose players as game mode,please Enter Player2 Name!"
#define Name2_msg "Enter Player2 name : "
#define error_name_msg "Please enter player name !"

#endif

#ifndef cpu_c
#define cpu_c

/*

file:    cpu.c - CPU Moving
author:  feitian16
date:    2012-04-24
Version: 1.0.0
Email:   feitian16@gmail.com

*/


/*
Description of Interface :
When game need a CPU move it call cpumove() function.
Variables to access of function cpumove() :
grid[3][3] - array 3x3 of char, contain position of players chars
player2char - type : char, CPU player char
player1char - type : char, Player char

Important :
CPU need to make ONE  move of CPU Player
*/

#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "define.h"

#include "cpu.h"
#include "game.h"

//Code
void cpumove(void)
{
	int ok=1;
	gint i,j=0;
#if debug
	g_printf("CPU move \n");
#endif
	/*
	g_printf("j 0-15 ramdom:\n");
	srand(time(NULL));
	i=rand()%15;
	srand(time(NULL)%i);
	for(i=0;i<16;i++)
	{
	  //srand(time(NULL)/66);
	  //srand(15);
	  j=rand()%15;
	  printf("%d,",j);
	}
	printf("\n");
	i=0;
	j=0;
	*/
	
	//cpu random move ---test
	do
	{
		ok=0;		
		srand(time(NULL));
		i=rand()%15;
		srand(time(NULL)%100);
		//srand(15);
		j=rand()%15;
                if(grid[i][j]==player1char)
		    ok=1;
                grid[i][j]=player2char;
	}
	while(ok);	
	g_printf("cpu random move grid[%d][%d]: %c\n",i,j,grid[i][j]);
	
/*
//Added by Timur Salikhov aka Tim (Tima-S@yandex.ru)
for (i=0; i<3; i++) {
        if ((grid[i][0]==player2char) && (grid[i][1]==player2char) && (grid[i][2]==' ')) { j=2; goto CreateMove;}
        if ((grid[i][0]==player2char) && (grid[i][2]==player2char) && (grid[i][1]==' ')) { j=1; goto CreateMove;}
        if ((grid[i][2]==player2char) && (grid[i][1]==player2char) && (grid[i][0]==' ')) { j=0; goto CreateMove;}
	
        if ((grid[0][i]==player2char) && (grid[1][i]==player2char) && (grid[2][i]==' ')) { j=i; i=2; goto CreateMove;}
        if ((grid[0][i]==player2char) && (grid[2][i]==player2char) && (grid[1][i]==' ')) { j=i; i=1; goto CreateMove;}
        if ((grid[2][i]==player2char) && (grid[1][i]==player2char) && (grid[0][i]==' ')) { j=i; i=0; goto CreateMove;}
	}
if ((grid[0][0]==player2char) && (grid[1][1]==player2char) && (grid[2][2]==' ')) { i=2; j=2; goto CreateMove;}
if ((grid[2][2]==player2char) && (grid[1][1]==player2char) && (grid[0][0]==' ')) { i=0; j=0; goto CreateMove;}
if ((grid[0][0]==player2char) && (grid[2][2]==player2char) && (grid[1][1]==' ')) { i=1; j=1; goto CreateMove;}

if ((grid[0][2]==player2char) && (grid[1][1]==player2char) && (grid[2][0]==' ')) { i=2; j=0; goto CreateMove;}
if ((grid[2][0]==player2char) && (grid[1][1]==player2char) && (grid[0][2]==' ')) { i=0; j=2; goto CreateMove;}
if ((grid[0][2]==player2char) && (grid[2][0]==player2char) && (grid[1][1]==' ')) { i=1; j=1; goto CreateMove;}

for (i=0; i<3; i++) {
        if ((grid[i][0]==player1char) && (grid[i][1]==player1char) && (grid[i][2]==' ')) { j=2; goto CreateMove;}
        if ((grid[i][0]==player1char) && (grid[i][2]==player1char) && (grid[i][1]==' ')) { j=1; goto CreateMove;}
        if ((grid[i][2]==player1char) && (grid[i][1]==player1char) && (grid[i][0]==' ')) { j=0; goto CreateMove;}
	
        if ((grid[0][i]==player1char) && (grid[1][i]==player1char) && (grid[2][i]==' ')) { j=i; i=2; goto CreateMove;}
        if ((grid[0][i]==player1char) && (grid[2][i]==player1char) && (grid[1][i]==' ')) { j=i; i=1; goto CreateMove;}
        if ((grid[2][i]==player1char) && (grid[1][i]==player1char) && (grid[0][i]==' ')) { j=i; i=0; goto CreateMove;}
	}
if ((grid[0][0]==player1char) && (grid[1][1]==player1char) && (grid[2][2]==' ')) { i=2; j=2; goto CreateMove;}
if ((grid[2][2]==player1char) && (grid[1][1]==player1char) && (grid[0][0]==' ')) { i=0; j=0; goto CreateMove;}
if ((grid[0][0]==player1char) && (grid[2][2]==player1char) && (grid[1][1]==' ')) { i=1; j=1; goto CreateMove;}

if ((grid[0][2]==player1char) && (grid[1][1]==player1char) && (grid[2][0]==' ')) { i=2; j=0; goto CreateMove;}
if ((grid[2][0]==player1char) && (grid[1][1]==player1char) && (grid[0][2]==' ')) { i=0; j=2; goto CreateMove;}
if ((grid[0][2]==player1char) && (grid[2][0]==player1char) && (grid[1][1]==' ')) { i=1; j=1; goto CreateMove;}
// End of Tim's code	

//If move was not created then move create rand... 
do {
        i=rand()%3;
        j=rand()%3;
     } while (grid[i][j]!=' ');

        
CreateMove: //Added by Tim. This label needs by Tim's code.
        grid[i][j]=player2char;
*/

}
#endif

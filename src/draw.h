#ifndef draw_h
#define draw_h

/*

file:    draw.h - header file 
author:  feitian16
date:    2012-04-24
Version: 1.0.0
Email:   feitian16@gmail.com

*/

/*
Description of Interface :
When game need a CPU move it call cpumove() function.
Variables to access of function cpumove() :
grid[3][3] - array 3x3 of char, contain position of players chars
cpuchar - type : char, CPU player char
usrchar - type : char, Player char

Important :
CPU need to make ONE  move of CPU Player
*/

void drawGrid(void);
void drawX(int, int);
void drawO(int,int);

#endif

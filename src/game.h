#ifndef game_h
#define game_h

/*

file:    game.h - header file
author:  feitian16
date:    2012-04-24
Version: 1.0.0
Email:   feitian16@gmail.com

*/





GtkWidget *window;//Main window
GtkWidget *ebox;//Read events form user
GtkWidget *darea;//Drawing area

GtkWidget *status_bar;
//GtkStatusbar *statusbar;
int stbar_id;

GtkWidget *menu_bar; //Menu bar
GtkWidget *vbox;//Main window Box : store menu and ebox


GdkGC *Xcolor,*Ocolor;//Colors for X and O
GdkColor color;//Temp color

gchar user1name[10];//Player1 Name
gchar user2name[10];//Player2 Name
gchar player_win[20];//User win string

/*
extern gint scaleX;//X scale
extern gint scaleO;//O scale
extern gint scaleGrid;//table scale
extern gchar usrchar,cpuchar;//Players Chars (default values)*/
/*
#ifndef GLOBAL
#define GLOBAL

gint scaleX;
gint scaleO;
gint scaleGrid;
gchar usrchar,cpuchar;

#endif*/

gint scaleX;//X scale
gint scaleO;//O scale
gint scaleGrid;//table scale
gchar player1char,player2char;//Players Chars (default values)
gint gameMode;//GameMode： 0 --- Player VS PC; 1--- Player1 VS Player2

gchar grid[15][15];;//Main table : ' ' - Free Zone; 'X' - Zone used by X; 'O'- Zone used by O

void setAI(GtkWidget *widget,gpointer data);
void setPlayers(GtkWidget *widget,gpointer data);
void setX(GtkWidget*,gpointer);
void setO(GtkWidget*,gpointer);
void login(void);

int confirm_exit_dialog(void);
void destroy(GtkWidget *,gpointer);
int checkwin(void);
void init_game(void);
void initcolors(void);
void playermove(gint,gint,gchar);
int showresult(void);

gint delete_event(GtkWidget *widget,GdkEvent *event,gpointer data);
gboolean redraw(void);
gboolean Game_action(GtkWidget *event_box,GdkEventButton *event,gpointer data);
gboolean Game_actionAI(gint,gint);
gboolean Game_actionPlayers(gint,gint);

#endif

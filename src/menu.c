#ifndef menu_c
#define menu_c

/*

file:    menu.c - manage main menu actions
author:  feitian16
date:    2012-10-21
Version: 1.0.0
Email:   feitian16@gmail.com

*/


#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

#include "menu.h"
#include "game.h"
#include "define.h"
#include "build.h"

//Confirm NEW Game dialog
#define new_game_title "New Game confirm"
#define new_game_str "Confirm new game"

//Option Dialog
#define option_title "Game Option"

/*
//About dialog
#define about_title "About"
#define about_str "<span size=\"x-large\">"release_version"</span>\n<tt>Build : "build"\nBuild Date: "build_date"\nCopyright (C) 2012 feitian16\nGobangC is free software :\nsee LICENSE file for license information</tt>\n"

//About website dialog
#define about_website_title "About website"
#define about_website_str "<span size=\"x-large\">"release_version"</span>\n<tt>Build : "build"\nProject home page :\n<b>http://gobangft.sourceforge.net</b></tt>\n"
*/
/*
//About dialog
#define about_title "About"
#define about_str "<span size=\"x-large\">"release_version"</span>\n<tt>Build : "build"\nCopyright (C) 2006 Obada Denis\nTicTacToeGTK is free software :\nsee LICENSE file for license information</tt>\nProject home page : <b>http://tictactoegtk.sourceforge.net</b>"

#define about_website_title "About website"
#define about_website_str "<span size=\"x-large\">"release_version"</span>\n<tt>Build : "build"\n</tt>\nProject home page :\n<b>http://gobangft.sourceforge.net</b>"
*/

//About dialog
#define about_title "About"
#define about_str "<span size=\"x-large\">"release_version"</span>\n<tt>Build : "build"\nCopyright (C) 2006 Obada Denis\nTicTacToeGTK is free software :\nsee LICENSE file for license information</tt>\nProject home page : <b>http://tictactoegtk.sourceforge.net</b>"

#define about_website_title "About website"
#define about_website_str "<span size=\"x-large\">"release_version"</span>\n<tt>Build : "build"\n</tt>\nProject home page :\n<b>http://gobangft.sourceforge.net</b>"

//Data structures
GSList *Ogrup1,*Ogrup2; //Option group
GtkWidget *radio11,*radio12,*radio21,*radio22; //Radio items


//Data structures
static GtkItemFactoryEntry menu_items[] = {
  { "/_Game",         NULL,         NULL,           0, "<Branch>" },
  { "/Game/_New Game","<control>N", callnewgame,           0, "<Item>" },
  { "/Game/filesep",  NULL,	    NULL,	    0, "<Separator>"},
  { "/Game/_Option",     "<control>O", calloption,  0, "<Item>"},
  { "/Game/filesep",  NULL,	    NULL,	    0, "<Separator>"},
  { "/Game/_Quit",     "<control>Q", callexit,  0, "<Item>"},
  { "/_Help",         NULL,         NULL,           0, "<LastBranch>" },
  { "/_Help/_About",   NULL,        call_about,           0, "<Item>" },
  { "/_Help/_About website",   NULL, call_about_website,  0, "<Item>" },
};

static gint nmenu_items = sizeof (menu_items) / sizeof (menu_items[0]);


//Code
GtkWidget *get_menubar_menu( GtkWidget  *window)
{
  GtkItemFactory *item_factory;
  GtkAccelGroup *accel_group;
  
  accel_group = gtk_accel_group_new ();
  //Create menu bar
  item_factory = gtk_item_factory_new (GTK_TYPE_MENU_BAR, "<main>",
                                       accel_group);
  //Create menu items
  gtk_item_factory_create_items (item_factory, nmenu_items, menu_items, NULL); 
  
  gtk_window_add_accel_group (GTK_WINDOW (window), accel_group);

  return gtk_item_factory_get_widget (item_factory, "<main>");
}

void callexit( GtkWidget *w,gpointer   data )
{
  //Call Exit confirm dialog and process result
  #if debug
  g_printf("callexit()\n");
  #endif
  if (confirm_exit_dialog())
	  gtk_main_quit();
}

void callnewgame(GtkWidget *w,gpointer data)
{
  //Call NewGame confirm dialog and process result
  #if debug
  g_printf("callnewgame()\n");
  #endif
  if (newgame_confirm()) 
  {
      init_game();
      login();
      redraw();
      //redraw(GtkWidget *,GdkEventExpose *,gpointer);
  }

}

gint newgame_confirm()
{
  //Show NewGame confirm dialog
  GtkWidget *dialog;
  GtkWidget *label;
  gint result;

  #if debug
  g_printf("newgame_confirm()\n");
  #endif

  //Create dialog
  dialog=gtk_dialog_new_with_buttons(new_game_title,GTK_WINDOW(window),
				     GTK_DIALOG_MODAL,GTK_STOCK_YES,
				     GTK_RESPONSE_ACCEPT,GTK_STOCK_NO,
				     GTK_RESPONSE_REJECT,NULL);
  gtk_window_set_policy(GTK_WINDOW(dialog),FALSE,FALSE,FALSE);
  //Create label
  label=gtk_label_new(new_game_str);
  gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox),label);
  //Show dialog
  gtk_widget_show_all(dialog);
  if (gtk_dialog_run(GTK_DIALOG(dialog))==GTK_RESPONSE_ACCEPT)
	  result=1;
	  else
	  result=0;

  #if debug
  g_printf("result=%d\n",result);
  #endif

  gtk_widget_destroy(dialog);
  return result;
}

void calloption( GtkWidget *w,gpointer data)
{
    //Option Dialog widgets
    GtkWidget *Odialog; //Option dialog
    GtkWidget *Olabel1,*Olabel2,*Olabel3,*Olabel4,*Olabel5; //Option dialog labels
    GtkWidget *Oedit1,*Oedit2;//Edit box
    GtkWidget *Otable;//Option table

    GtkWidget *msg;//Login Message
    gchar username[10];
    int ok=1;

   Odialog = gtk_dialog_new_with_buttons (option_title,GTK_WINDOW(window),GTK_DIALOG_MODAL,
                                           GTK_STOCK_OK,GTK_RESPONSE_ACCEPT,NULL);
    gtk_window_set_resizable(GTK_WINDOW(Odialog),TRUE);

    gtk_widget_set_size_request(Odialog,450,300);
    gtk_container_set_border_width (GTK_CONTAINER (Odialog), 5);

   Olabel1 = gtk_label_new(Name_msg);
   Olabel2 = gtk_label_new(Mode_msg);
   Olabel3 = gtk_label_new(Chess_msg);
   Olabel4 = gtk_label_new(Player2name_msg);
   Olabel5 = gtk_label_new(Name2_msg);

   Oedit1=gtk_entry_new_with_max_length(10);
   gtk_entry_set_text(GTK_ENTRY(Oedit1),"Michael");
   Oedit2=gtk_entry_new_with_max_length(10);
   gtk_entry_set_text(GTK_ENTRY(Oedit2),"Susan");

   radio11 = gtk_radio_button_new_with_label (NULL, "AI");
   Ogrup1 = gtk_radio_button_get_group(GTK_RADIO_BUTTON(radio11));
   radio12 = gtk_radio_button_new_with_label( Ogrup1, "Players");

   g_signal_connect(G_OBJECT(radio11),"clicked",G_CALLBACK(setAI),NULL);
   g_signal_connect(G_OBJECT(radio12),"clicked",G_CALLBACK(setPlayers),NULL);

    radio21 = gtk_radio_button_new_with_label (NULL, "X");
    Ogrup2 = gtk_radio_button_get_group(GTK_RADIO_BUTTON(radio21));
    radio22 = gtk_radio_button_new_with_label( Ogrup2, "O");

    g_signal_connect(G_OBJECT(radio21),"clicked",G_CALLBACK(setX),NULL);
    g_signal_connect(G_OBJECT(radio22),"clicked",G_CALLBACK(setO),NULL);

    Otable=gtk_table_new(5,3,FALSE);

    gtk_table_attach_defaults (GTK_TABLE (Otable), Olabel1, 0, 1, 0, 1);
    gtk_table_attach_defaults (GTK_TABLE (Otable), Olabel2, 0, 1, 1, 2);
    gtk_table_attach_defaults (GTK_TABLE (Otable), Olabel3, 0, 1, 2, 3);
    gtk_table_attach_defaults (GTK_TABLE (Otable), Oedit1, 1, 3, 0, 1);

    gtk_table_attach_defaults (GTK_TABLE (Otable), radio11, 1, 2, 1, 2);
    gtk_table_attach_defaults (GTK_TABLE (Otable), radio12, 2, 3, 1, 2);
    gtk_table_attach_defaults (GTK_TABLE (Otable), radio21, 1, 2, 2, 3);
    gtk_table_attach_defaults (GTK_TABLE (Otable), radio22, 2, 3, 2, 3);

    gtk_table_attach_defaults (GTK_TABLE (Otable), Olabel4, 0, 3, 3, 4);
    gtk_table_attach_defaults (GTK_TABLE (Otable), Olabel5, 0, 1, 4, 5);
    gtk_table_attach_defaults (GTK_TABLE (Otable), Oedit2, 1, 3, 4, 5);

    gtk_container_add (GTK_CONTAINER (GTK_DIALOG(Odialog)->vbox),Otable);
    //Show dialog
    gtk_widget_show_all(Odialog);
    //Process events form dialog
    do
    {
      ok=0;
      //Run dialog
      while( gtk_dialog_run (GTK_DIALOG (Odialog))!=GTK_RESPONSE_ACCEPT) ;
      //Get infor from Dialog
      strcpy(username,gtk_entry_get_text(GTK_ENTRY(Oedit1)));
      //If UserName is Empty
      if (strlen(username)==0)
      {
            //Show Warning Message
            msg=gtk_message_dialog_new (GTK_WINDOW(window),
                                        GTK_DIALOG_MODAL,
                                        GTK_MESSAGE_INFO,
                                        GTK_BUTTONS_OK,
                                        error_name_msg);
            gtk_dialog_run(GTK_DIALOG(msg));
            gtk_widget_destroy(msg);
            ok=1; //Loop will not stop
      }
      else
      {
        //Register user name
        strcpy(user1name,username);
      }

        #if debug
        printf("User Name : %s\n",username);
        #endif

        if(gameMode==1)
        {
            strcpy(username,gtk_entry_get_text(GTK_ENTRY(Oedit2)));
            if(strlen(username)==0)
            {
                //Show Warning Message
                msg=gtk_message_dialog_new (GTK_WINDOW(window),
                                            GTK_DIALOG_MODAL,
                                            GTK_MESSAGE_INFO,
                                            GTK_BUTTONS_OK,
                                            error_name_msg);
                gtk_dialog_run(GTK_DIALOG(msg));
                gtk_widget_destroy(msg);
                ok=1; //Loop will not stop
            }
            else
            {
              //Register user name
              strcpy(user2name,username);
            }

            #if debug
            printf("Player2 Name : %s\n",username);
            #endif
        }
    }
    while(ok);
    //Destroy dialog
    gtk_widget_destroy(Odialog);
}

void call_about(GtkWidget *w,gpointer data)
{
  //About Dialog
  GtkWidget *dialog;
  GtkWidget *label;
  gint result;

  #if debug
  g_printf("call_about()\n");
  #endif

  //Create dialog
  dialog=gtk_dialog_new_with_buttons(about_title,GTK_WINDOW(window),GTK_DIALOG_MODAL,GTK_STOCK_OK,GTK_RESPONSE_ACCEPT,NULL);
  gtk_window_set_policy(GTK_WINDOW(dialog),FALSE,FALSE,FALSE);
  gtk_widget_set_size_request(dialog,400,200);
  //Create label
  label=gtk_label_new(NULL);
  gtk_label_set_markup (GTK_LABEL (label),about_str);
  gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox),label);
  //Show dialog
  gtk_widget_show_all(dialog);
  gtk_dialog_run(GTK_DIALOG(dialog));
  gtk_widget_destroy(dialog);
}

void call_about_website(GtkWidget *w,gpointer data)
{
  //About website Dialog
  GtkWidget *dialog;
  GtkWidget *label;
  gint result;

  #if debug
  g_printf("call_about_website()\n");
  #endif

  //Create dialog
  dialog=gtk_dialog_new_with_buttons(about_website_title,GTK_WINDOW(window),GTK_DIALOG_MODAL,GTK_STOCK_OK,GTK_RESPONSE_ACCEPT,NULL);
  gtk_window_set_policy(GTK_WINDOW(dialog),FALSE,FALSE,FALSE);
  gtk_widget_set_size_request(dialog,300,200);
  //Create label
  label=gtk_label_new(NULL);
  gtk_label_set_markup (GTK_LABEL (label),about_website_str);
  gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox),label);
  //Show dialog
  gtk_widget_show_all(dialog);
  gtk_dialog_run(GTK_DIALOG(dialog));
  gtk_widget_destroy(dialog);
}

#endif

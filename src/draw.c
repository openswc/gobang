#ifndef draw_c
#define draw_c

/*

file:    draw.c - drawing objects 
author:  feitian16
date:    2012-04-24
Version: 1.0.0
Email:   feitian16@gmail.com

*/

#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "draw.h"
#include "game.h"

//Code
void drawGrid(void)
//Draw Grid
{
      gint i,j;

      //Draw Background
      gdk_draw_rectangle(darea->window,darea->style->white_gc,TRUE,0,0,600,600);

      //Draw Border
      gdk_draw_rectangle(darea->window,darea->style->black_gc,FALSE,0,0,600,600);

      //Draw lines
      for(i=1;i<15;i++)
      {
        //Draw Vertical lines
        gdk_draw_line(darea->window,darea->style->black_gc,scaleGrid*i,0,scaleGrid*i,scaleGrid*15);
        //Draw orizontal lines
        gdk_draw_line(darea->window,darea->style->black_gc,0,scaleGrid*i,scaleGrid*15,scaleGrid*i);
      }

      //Draw chars (X or O if needed)
      for(i=0;i<15;i++)
      {
            for(j=0;j<15;j++)
            {
              if (grid[i][j]=='X')//Draw X
                      drawX((scaleGrid/2)+(i*(scaleGrid)),(scaleGrid/2)+(j*(scaleGrid)));
                      //g_printf("draw X grid[%d][%d]: %s\n",i,j,grid);
              if (grid[i][j]=='O')//Draw O
                      drawO((scaleGrid/2)+(i*(scaleGrid)),(scaleGrid/2)+(j*(scaleGrid)));
                      //g_printf("draw O grid[%d][%d]: %s\n",i,j,grid);
            }
      }
     g_printf("draw grid OK\n");
}

void drawX(gint i,gint j)
//Draw X
{
  int a;
  for (a=1;a<=5;a++)
  {
    gdk_draw_line(darea->window,Xcolor,i-scaleX+a,j-scaleX,i+scaleX-a,j+scaleX);
    gdk_draw_line(darea->window,Xcolor,i-scaleX-a,j+scaleX,i+scaleX+a,j-scaleX);
  }
}

void drawO(gint i,gint j)
//Draw O
{
  int a;
  for(a=-2;a<=3;a++)
  {
    gdk_draw_arc(darea->window,Ocolor,FALSE,i-scaleO+a,j-scaleO-a,scaleO*2,scaleO*2,0, 64 * 360);
  }
}
#endif
